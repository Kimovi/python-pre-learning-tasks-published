import re
def vowel_swapper(string):
    # ==============
    # Your code hered
    return string.lower().replace("a", "4",2).replace("4","a",1).replace("e", "3",2).replace("3","e",1).replace("i", "!",2).replace("!", "i",1).replace("o", "ooo", 2).replace("ooo","o",1).replace("u", "|_|",2).replace("|_|","u",1)
    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
